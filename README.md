# vue-template

Шблон для Frontend проектов. Vue.js, Pug, SoffeeScript, SASS.

## Как билдить через npm. 

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Как этот шаблон собирался
В основе лежит стандартный [webpack-шаблон](https://vuejs-templates.github.io/webpack/) Vue.js.
Установлены пакеты для поддержки препроцессоров Pug, CoffeeScript и SCSS.

```bash
# install yanr
$ yarn global add vue-cli

# init default webpack template
# 'vue' binary is in $HOME/.yarn/bin
$ vue init webpack vue-template
$ cd vue-template

# add coffeescript support
$ yarn add coffeescript coffee-loader --dev

# add pug support
$ yarn add pug pug-loader --dev

# add scss support
$ yarn add sass-loader node-sass style-loader --dev
```